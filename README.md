AWS ECS Jenkins
===============
This role is part of the [Mirabeau Cloud Framework](https://gitlab.com/mirabeau/cloud-framework/)

Create Jenkins ECS services with CloudFormation.

AWS resources that will be created are:
 * RDS SecurityGroup
 * TaskDefinitions
   * Jenkins master
   * Java slave
 * ELB Target group (if target ALB is given)
 * ELB Listener 80 Rule (if target ALB is given)
 * ELB Listener 443 Rule (if target ALB is given AND enable_443 is set to True - requires your ALB to have this listener)
 * Route53 record (if target ALB is given)

The role itself does not create an ECS cluster but takes an existing cluster name as a parameter for deployment.
Also note that while the Listener Rule(s) will be created it does not include any security group ingress rules since these are very customer specific.
You will need to add those yourself - i.e. in the `env-securitygroups-ingress` role.

Note that Jenkins requires persistent storage for it's home dir. You can choose which backend should be used, EFS or EBS.

When EBS is used, the RexRay plugin needs to be enabled for the aws-ecs-cluster role.

When EFS is used, EFS should be enabled for the aws-ecs-cluster role.
*One time manual action required*
Log in to a ECS server which has the EFS volume mounted:
```bash
chown -R 1000:1000 /efs/*-ecs-jenkins/jenkins/
chmod -R 755 /efs/*-ecs-jenkins/jenkins/
```

Requirements
------------
Ansible version 2.5.4 or higher  
Python 2.7.x  
Pip 18.x or higher (Python 2.7)

Required python modules:
* boto
* boto3
* awscli
* docker

Dependencies
------------
 * aws-lambda
 * aws-iam
 * aws-vpc
 * aws-vpc-lambda
 * aws-securitygroups
 * aws-hostedzone
 * aws-ecs-cluster
 * aws-ecs-alb

Role Variables
--------------
### _General_
The following params should be available for Ansible during the rollout of this role:
```yaml
aws_region      : <aws region, eg: eu-west-1>
owner           : <owner, eg: mirabeau>
account_name    : <aws account name>
account_abbr    : <aws account generic environment>
environment_type: <environment>
environment_abbr: <environment abbriviation>
```

Role Defaults
-------------
```yaml
---
create_changeset     : True
debug                : False
cloudformation_tags  : {}
tag_prefix           : "mcf"

aws_ecs_jenkins_params:
  create_changeset: "{{ create_changeset }}"
  debug           : "{{ debug }}"

  alb_role              : "ecs-alb"
  elb_scheme            : "internal"
  enable_443            : False  # NOTE: you need a 443 listener on your ALB to make this work
  storage_type          : "efs"  # can be either efs or ebs
  ecs_cluster_stack_name: "{{ slicename | default(environment_abbr) }}-{{ aws_ecs_cluster_params.cluster_name }}"

  master:
    image          : "{{ account_id }}.dkr.ecr.{{ aws_region }}.amazonaws.com/jenkins/master"
    image_tag      : "latest"
    memory         : 1024
  java_slave:
    image          : "{{ account_id }}.dkr.ecr.{{ aws_region }}.amazonaws.com/jenkins/java-slave"
    image_tag      : "latest"
    desiredcount   : 1
    memory         : 1024

  repositories:
    - master
    - java-slave
```

Example Playbooks
-----------------
Build docker image and push to ECR
```yaml
---
- hosts: localhost
  tasks:
    - name: "jenkins | docker | build and push"
      include_role:
        name: aws-ecs-jenkins
        tasks_from: build-docker
      tags:
        - aws:ecs:jenkins
        - jenkins
```

Rollout the aws-ecs-jenkins files with defaults
```yaml
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    tag_prefix      : "mcf"
    aws_region      : "eu-west-1"
    owner           : "myself"
    account_name    : "my-dta"
    account_abbr    : "dta"
    environment_type: "test"
    environment_abbr: "tst"
    internal_route53:
      domainname: my.cloud
    aws_vpc_params:
      network               : "10.10.10.0/24"
      public_subnet_weight  : 1
      private_subnet_weight : 3
      database_subnet_weight: 1
  pre_tasks:
    - name: "Get latest AWS AMI's"
      include_role:
        name: aws-utils
        tasks_from: get_aws_amis
    - name: "Docker build and push"
      include_role:
        name: aws-ecs-jenkins
        tasks_from: build-docker
  roles:
    - aws-setup
    - aws-iam
    - aws-vpc
    - env-acl
    - aws-vpc-lambda
    - aws-securitygroups
    - aws-lambda
    - aws-ecs-cluster
    - aws-ecs-alb
    - aws-ecs-jenkins
```
License
-------
GPLv3

Author Information
------------------
Lotte-Sara Laan <llaan@mirabeau.nl>  
Wouter de Geus <wdegeus@mirabeau.nl>  
Rob Reus <rreus@mirabeau.nl>
